﻿using System;

namespace Zarlo.Cosmos.FileSystems.NTFS.Model.Enums
{
    [Flags]
    public enum MFTEAFlags
    {
        NeedEA = 0x80
    }
}