namespace Zarlo.Cosmos.Threading;

public delegate void ParameterizedThreadStart(object? obj);

public delegate void ThreadStart();
