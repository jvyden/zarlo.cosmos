namespace Zarlo.Cosmos.Logger;

public enum LogLevel
{
    Info,
    Error,
    Trace,
    Debug,
    Exception
}